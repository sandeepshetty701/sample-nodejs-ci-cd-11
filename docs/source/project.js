/**
 *
 * @api {post} /api/v1/projects     Create a new Project
 * @apiName Create Project
 * @apiGroup Project Management
 * @apiVersion  1.0.0
*
*
* @apiParamExample  {json} Request-Example:
* {
  "name": "Test Project",
  "location": [17.1234, 78.4321],
  "address": "Road No. 63, Jubliee Hills, Hyderabad. Telangana",
  "scanDetails": {
    "exterior": {
      "name": "Exterior",
      "area": {
        "quantity": 500,
        "units": "sft."
      },
      "repeat": {
        "count": 2,
        "frequency": "Weekly"
      }
    }
  },
  "projectType": "Villa"
}
*
* @apiExample Example usage:
*    https://construct.api.dev.3rdi.xyz/api/v1/projects
*
* @apiSuccess {Object} Project created project
*
* @apiSuccessExample Success-Response:
{
  "success": true,
  "result": {
    "location": [
      17.1234,
      78.4321
    ],
    "projectType": "Villa",
    "projects": [],
    "tilesets": [],
    "_id": "5e4138daf6780a01ec47be7a",
    "name": "Test Project 2",
    "address": "Road No. 63, Jubliee Hills, Hyderabad. Telangana",
    "scanDetails": {
      "exterior": {
        "_id": "5e4138daf6780a01ec47be7b",
        "name": "Exterior",
        "area": {
          "quantity": 500,
          "units": "sft."
        },
        "repeat": {
          "count": 2,
          "frequency": "Weekly"
        }
      }
    },
    "snapshot": [],
    "createdBy": "5e40c8d8e5b66204bffd1769",
    "owner": "5e40c8d8e5b66204bffd1769",
    "createdAt": "2020-02-10T11:04:58.836Z",
    "updatedAt": "2020-02-10T11:04:58.836Z",
    "__v": 0,
    "progress": {
      "date": "2020-02-10T11:04:58.916Z",
      "planned": 0,
      "actual": 0
    },
    "id": "5e4138daf6780a01ec47be7a"
  },
  "status": "success"
}
*
*  @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} NoMissionEntry
*    HTTP/1.1 400 Invalid mission Id
*    {
*       success: false,
*       status: 'Error'
*    }
*/

/**
 *
 * @api {patch} /api/mission-management/job/:jobId update a step of a Job
 * @apiName update_stepOf_job
 * @apiGroup Job
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} jobId Job's id 
* @apiParam  {String} stepName Name of the step. If updating a child_step, separate step names by '#'.
* @apiParam  {String} stepData.action action Action to be performed on Step.ONLY 'START' or 'END'.
* @apiParam  {Date} stepData.time start time of the step
* @apiParam  {Object} stepData.data Any data to be passed to this step.
*
* @apiParamExample  {json} Request-Example:
{
	"stepName" : "IMAGE_PROCESS#PC_POST#LASZIP",
    "action" : "START",
    "stepData" : {
        "time" : "2019-03-20 14:23:45",
        "data" : {
		    "s3_location" : "s3://129/pointcloud"
	    }
    }
}
*
* @apiExample Example usage:
*    http://52.66.177.173:8001/api/mission-management/job/5c7cf3e0cc37fb31c87a2dc3
*
* @apiSuccess {Object} job updated a step of job
*
* @apiSuccessExample Success-Response:
{
    "steps": [
        {
            "name": "FLIGHT",
            "assignee": "5ab281dc7658e5346c6d2781",
            "date": "2019-02-26T18:20:50.000Z"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP",
                            "data": {
                                "s3_location": "s3://129/pointcloud"
                            },
                            "start_time": "2019-03-20T08:53:45.000Z",
                            "status": "INPROGRESS"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ],
                    "start_time": "2019-03-20T08:53:45.000Z",
                    "status": "INPROGRESS"
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ],
            "start_time": "2019-03-20T08:53:45.000Z",
            "status": "INPROGRESS"
        }
    ],
    "_id": "5c7cf3e0cc37fb31c87a2dc3",
    "mission": "5c74ec867901b42fb15afa22",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "createdAt": "2019-03-04T09:46:08.348Z",
    "updatedAt": "2019-03-04T10:14:48.289Z",
    "__v": 0,
    "currentStep": {
        "name": "FLIGHT",
        "status": "Not started",
        "assignee": "5ab281dc7658e5346c6d2781"
    },
    "id": "5c7cf3e0cc37fb31c87a2dc3"
}
*
* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} JobNotFound
*    HTTP/1.1 400 Cant update job as job with given id does not exist
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} InvalidActionOrNoActionEntry
*    HTTP/1.1 400 Invalid action or No Action Entry. Accepted actions - 'START' or 'END'
*    {
*       success: false,
*       status: 'Error'
*    }
*/

/**
 *
 * @api {patch} /api/mission-management/job/:jobId/update update a Job
 * @apiName update_job
 * @apiGroup Job
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} jobId Job's id 
* @apiParam  {String} assignee UserId to whom the Flight step in the job needs to be assigned to.
* @apiParam  {Date} date Date by which the Job needs to be completed
*
* @apiParamExample  {json} Request-Example:
{
	"assignee" : "5ab281dc7658e5346c6d2782",
	"date" : "2019-02-28 10:05:50"
}
* 
* @apiExample Example usage:
* http://52.66.177.173:8001/api/mission-management/job/5c7d1ed911876833103ffbdc/update
*
* @apiSuccess {Object} job updated job
*
* @apiSuccessExample Success-Response:
{
    "steps": [
        {
            "name": "FLIGHT",
            "assignee": "5ab281dc7658e5346c6d2782",
            "date": "2019-02-28T04:35:50.000Z"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c7fb17c30a1e07452047bca",
    "mission": "5c74ec867901b42fb15afa22",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "createdAt": "2019-03-06T11:39:40.282Z",
    "updatedAt": "2019-03-07T07:34:25.417Z",
    "__v": 0,
    "currentStep": {
        "name": "FLIGHT",
        "status": "Not started",
        "assignee": "5ab281dc7658e5346c6d2782"
    },
    "id": "5c7fb17c30a1e07452047bca"
}
*
* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} JobNotFound
*    HTTP/1.1 400 Cant update job as job with given id does not exist
*    {
*       success: false,
*       status: 'Error'
*    }
*/

/**
 *
 * @api {get} /api/mission-management/job/
 * @apiName list_jobs
 * @apiGroup Job
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} project Projects's id
* @apiParam  {uuid} mission Missions's id
* @apiParam  {uuid} assignee Assignee's id
* @apiParam  {String} step Step's Name
* @apiParam  {String} status Step's Status
* @apiParam  {String} from From Date
* @apiParam  {String} to To Date 
*
* @apiParamExample {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/job/?step=FLIGHT&status=INPROGRESS&from=2018-03-05 15:35:00&to=2019-03-05 15:35:00
* http://52.66.177.173:8001/api/mission-management/job/?step=FLIGHT&status=INPROGRESS
* http://52.66.177.173:8001/api/mission-management/job/
* http://52.66.177.173:8001/api/mission-management/job/?assignee=5c626cdcc9b706258cd34636
* http://52.66.177.173:8001/api/mission-management/job/?project=6c626cdcc6z406258cd34639
* http://52.66.177.173:8001/api/mission-management/job/?mission=9c626cdcc9b706258cd34939
* http://52.66.177.173:8001/api/mission-management/job/?project=6c626cdcc6z406258cd34639&mission=9c626cdcc9b706258cd34939&assignee=5c626cdcc9b706258cd34636
*
* @apiSuccess {Object} job list jobs
*
* @apiSuccessExample Success-Response:
[
    {
        "steps": [
            {
                "name": "FLIGHT",
                "status": "INPROGRESS",
                "assignee": "5c626cdcc9b706258cd34636",
                "start_time": "1970-01-01T00:00:00.000Z"
            },
            {
                "name": "UPLOAD",
                "assignee": "5c626cdcc9b706258cd34637",
                "date": "1970-01-01T00:00:00.000Z"
            },
            {
                "name": "IMAGE_PROCESS",
                "child_steps": [
                    {
                        "name": "ODM_PROCESS"
                    },
                    {
                        "name": "PC_POST",
                        "child_steps": [
                            {
                                "name": "LASZIP"
                            },
                            {
                                "name": "SNIP_PC"
                            },
                            {
                                "name": "POTREE_CONV"
                            }
                        ]
                    },
                    {
                        "name": "ORTHO_POST",
                        "child_steps": [
                            {
                                "name": "SNIP_ORTHO"
                            },
                            {
                                "name": "ORTHO_TILES"
                            }
                        ]
                    },
                    {
                        "name": "CAMERAS"
                    },
                    {
                        "name": "THUMBNAILS"
                    },
                    {
                        "name": "UPLOAD_S3"
                    }
                ],
                "end_time": "1990-01-01T00:00:00.000Z"
            }
        ],
        "_id": "5c74f79bb401246b8718c3ab",
        "mission": "5c74ec867901b42fb15afa22",
        "project": "5abf81b88c0d217615448d01",
        "type": "DRONE",
        "createdAt": "2019-02-26T08:23:55.994Z",
        "updatedAt": "2019-02-26T08:23:55.994Z",
        "__v": 0,
        "currentStep": {
            "name": "FLIGHT",
            "status": "INPROGRESS",
            "assignee": "5c626cdcc9b706258cd34636"
        },
        "id": "5c74f79bb401246b8718c3ab"
    }
]
*/

/**
 *
 * @api {get} /api/mission-management/job/:jobId read a Job by Id
 * @apiName read_job
 * @apiGroup Job
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} jobId Job's id 
*
* @apiParamExample {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/job/5c750e427901b42fb15afa24
*
* @apiSuccess {Object} job read a job
*
* @apiSuccessExample Success-Response:
{
    "steps": [
        {
            "name": "FLIGHT",
            "assignee": "5ab281dc7658e5346c6d2781",
            "date": "2019-02-26T18:20:50.000Z"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP",
                            "data": {
                                "s3_location": "s3://129/pointcloud"
                            },
                            "start_time": "2019-02-26T18:20:50.000Z",
                            "status": "INPROGRESS"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ],
                    "start_time": "2019-02-26T18:20:50.000Z",
                    "status": "INPROGRESS"
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ],
            "start_time": "2019-02-26T18:20:50.000Z",
            "status": "INPROGRESS"
        }
    ],
    "_id": "5c7cf3e0cc37fb31c87a2dc3",
    "mission": "5c74ec867901b42fb15afa22",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "createdAt": "2019-03-04T09:46:08.348Z",
    "updatedAt": "2019-03-04T10:14:48.289Z",
    "__v": 0,
    "currentStep": {
        "name": "FLIGHT",
        "status": "Not started",
        "assignee": "5ab281dc7658e5346c6d2781"
    },
    "id": "5c7cf3e0cc37fb31c87a2dc3"
}
*/

/**
 *
 * @api {delete} /api/mission-management/job/:jobId delete a Job by Id
 * @apiName delete_job
 * @apiGroup Job
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} jobId Job's id 
*
* @apiParamExample {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/job/5c74ed1b7901b42fb15afa23
*
* @apiSuccess {Object} job deleted a job
*
* @apiSuccessExample Success-Response:
{
    "steps": [
        {
            "name": "FLIGHT",
            "assignee": "5ab281dc7658e5346c6d2781",
            "date": "2019-02-26T00:00:00.000Z"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c74ed1b7901b42fb15afa23",
    "mission": "5c74ec867901b42fb15afa22",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "createdAt": "2019-02-26T07:39:07.984Z",
    "updatedAt": "2019-02-26T07:39:07.984Z",
    "__v": 0,
    "currentStep": {
        "name": "FLIGHT",
        "status": "Not started",
        "assignee": "5ab281dc7658e5346c6d2781"
    },
    "id": "5c74ed1b7901b42fb15afa23"
}
*/
