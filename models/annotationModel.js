'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var AnnotationSchema = new Schema({

  property: { //  object id of the property under which this annotation is created

    type: Schema.Types.ObjectId,
    ref: 'Property',
    required: true
  },

  entity: {
    type: String,
    trim: true
  },

  createdBy: { //  object id of the user who created this annotaiton

    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  type: { // type of the annotation (2D or 3D)
    type: String,
    trim: true,
    enum: ['2D', '3D'],
    default: '3D'
  },

  geometry: Object, // geometry of the annotation

  properties: Object // properties describing the annotation

}, { timestamps: true });

module.exports = mongoose.model('Annotation', AnnotationSchema);
