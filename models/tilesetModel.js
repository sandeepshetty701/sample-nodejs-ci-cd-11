'use strict';

require('dotenv').config();

var mongoose = require('mongoose'),
  GeoJSON = require('mongoose-geojson-schema'),
  Schema = mongoose.Schema;

var TilesetSchema = new Schema({

  name: { //  name of the tileset

    type: String,
    trim: true,
    required: true
  },

  description: { //  few lines description about the tileset

    type: String,
    trim: true
  },

  createdBy: { //  emailid of user who created this tileset

    type: Schema.Types.ObjectId,
    ref: 'User',
    trim: true,
    required: true
  },

  project: { //  object id of the project under which this tileset is created

    type: String,
    trim: true,
    required: true
  },

  thumbnail: { //  thumbnail image representing the tileset
    type: String
  },

  center: { // tileset default center
    type: [Number]
  },

  zoom: { // tileset default zoom

    type: Number,
    default: 17
  },

  pitch: { // tileset default pitch

    type: Number,
    default: 0
  },

  style: String, // tileset default style

  tilesetDate: { //  date of the tileset that should appear on the timeline

    type: Date,
    default: Date.now
  }

}, { timestamps: true });


module.exports = mongoose.model('Tileset', TilesetSchema);
