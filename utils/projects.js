var storage = require('../lib/storage/mongodb');

async function checkForPermission(token, organisationId, projectId, permission) {
    try {
        let userToken = await storage.getUserToken(token);
        if (userToken) {
            let permissions = [];
            if (organisationId) {
                permissions = await getUserOrganisationPermissions(userToken.user, organisationId);
            } else {
                permissions = await getUserProjectPermissions(userToken.user, projectId);
            }
            if (permissions.indexOf(permission) < 0) {
                throw ('403-|-You are not authorised to complete this action!')
            }
        } else {
            throw ('401-|-Please login to complete this action!')
        }
    } catch (err) {
        throw (err)
    }
}

async function getUserProjectPermissions(userId, projectId) {
    try {
        let project = await storage.projects.getProject({ _id: projectId, 'users.user': userId });
        if (!project) {
            throw ('403-|-You are not assigned to this project')
        }
        project = JSON.parse(JSON.stringify(project));

        let userRole;
        for (let i = 0; i < project.users.length; i++) {
            if (project.users[i].user == userId) {
                userRole = project.users[i].role;
                break;
            }
        }
        let role = await storage.roles.getRole({ _id: userRole });
        return role.permissions;
    } catch (err) {
        throw (err)
    }
}

async function getUserOrganisationPermissions(userId, organisationId) {
    try {
        let user = await storage.users.getUser({ _id: userId });
        user = JSON.parse(JSON.stringify(user));
        let userRole;
        for (let i = 0; i < user.organisations.length; i++) {
            if (user.organisations[i].organisation === organisationId) {
                userRole = user.organisations[i].role;
                break;
            }
        }
        let role = await storage.roles.getRole({ _id: userRole });
        return role.permissions;
    } catch (err) {
        throw (err)
    }
}


module.exports = {
    checkForPermission: checkForPermission
}