'use strict';

var express = require('express');

var multer = require('multer');

const router = express.Router({mergeParams: true});

var errorHandler = require('../validators/errorHandler');

var middlewares = require('../middlewares');

var organisationController = require('../controllers/organisationController');

var storage = multer.memoryStorage({
    destination: function (req, file, callback) {
        callback(null, '');
    }
});

var multipleUpload = multer({ storage: storage, limits: { fileSize: 500 * 1024 * 1024 } }).single('file');

//  create organisation
router.post('/', organisationController.createOrganisation);

router.use(middlewares.authFunc.checkUserToken);

//  get all the organisations based on filters in query params
router.get('/', organisationController.getOrganisations, errorHandler.run);

//  get all the organisations for the user
router.get('/mine', organisationController.getMyOrganisations, errorHandler.run);

//  get organisation info
router.get('/:orgId', organisationController.getOrganisation, errorHandler.run);

//  update organisation info
router.put('/:orgId', organisationController.updateOrganisation, errorHandler.run);

//  upload organisation logo
router.put('/:orgId/upload-logo', multipleUpload, organisationController.uploadLogo, errorHandler.run);

//  assign organisation user
router.post('/:orgId/assign-user', organisationController.assignUser, errorHandler.run);

//  de-assign organisation user
router.delete('/:orgId/deassign-user', organisationController.deassignUser, errorHandler.run);

//  de-assign organisation user
router.put('/:orgId/change-role', organisationController.changeRole, errorHandler.run);

//  update organisation
router.put('/', organisationController.updateOrganisation, errorHandler.run);

//  delete organisation
router.delete('/', organisationController.deleteOrganisation, errorHandler.run);


module.exports = router;
