'use strict';

var express = require('express');

var router = express.Router();

var projectController = require('../controllers/projectController');

var activityRoutes = require('./activityRoutes');

var projectMngtValidator = require('../validators/projectManagement');

var errorHandler = require('../validators/errorHandler');

var multer = require('multer');

var storage = multer.memoryStorage({
    destination: function (req, file, callback) {
        callback(null, '');
    }
});

var multipleUpload = multer({ storage: storage, limits: { fileSize: 500 * 1024 * 1024 } }).single('file');

// List flat list of all projects assigned to me
router.get('/flat', errorHandler.run, projectController.listMyProjectsFlat);

// Get the project hierarchy
router.get('/hierarchy', errorHandler.run, projectController.getProjectHierarchy);

// Create a new project
router.post('/', errorHandler.run, projectController.createProject);

// Retrieve the project info whose id is sent in path params
router.get('/:id', projectController.getProject);

// Increase unread count for all users in the project
router.put('/:id/inc-unread-count', projectController.incUnreadCount)

// reset unread count for logged-in user for the project
router.put('/:id/reset-unread-count', projectController.resetUnreadCount)

// Retrieve the gantt info whose id is sent in path params
router.get('/:id/gantt', projectController.getProjectGantt);

// Upload the project-plan in given format (EXCEL template)
router.get('/:id/project-plan/download', projectController.downloadProjectPlan);

// Upload the project-plan in given format (EXCEL template)
router.post('/:id/project-plan/upload', multipleUpload, projectController.uploadProjectPlan);

// Get the project hierarchy
// router.get('/:id/hierarchy', projectController.get_project_hierarchy);

// Update the project whose id is sent in path params
router.put('/:id', projectMngtValidator.checkUpdateProject, errorHandler.run, projectController.updateProject);

// Delete the project whose id is sent in path params
router.delete('/:id', projectMngtValidator.checkDeleteProject, errorHandler.run, projectController.deleteProject);

// Assign a new user and role to a project
router.post('/:id/assign-user', errorHandler.run, projectController.assignUser);

// De-assign a new user and role to a project
router.put('/:id/deassign-user', errorHandler.run, projectController.deassignUser);

// Change role
router.put('/:id/change-role', errorHandler.run, projectController.changeRole);

//Activity routes related to projects
router.use('/:id/activities', activityRoutes);

module.exports = router;
